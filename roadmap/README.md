# Roadmap

This is the product roadmap for Micro Services, Inc.

## Overview

Micro is a developer first focused company. Our primary goal is to enable developers 
to build, share and collaborate on Micro services. We do this via an open source framework, 
runtime and a platform for Micro services development.

## Contents

- [Objectives](#objectives) - Objectives for the product
- [Timeline](#timeline) - Timelines of delivery

## Objectives

Our goals for 2020

- Bootstrap **M3O** - Micro as a Service - a fully managed microservices platform
- Onboard Users - Invite Micro Community first to build services on the platform
- Create Services - Create value add Micro services on the platform for users
- Launch Marketplace - Offer the ability to buy, sell and share Micro services 

## Timeline

Timeline, objectives and key results

- [2020](#2020-milestones)
  * [Q1](#q1-okrs) - Micro Platform - Product testing
  * [Q2](#q2-okrs) - M3O Platform MVP - Developer Tier
  * [Q3](#q3-okrs) - M3O Public Launch - Invite waitlist
  * [Q4](#q4-okrs) - M3O Team Tier - Collaboration features
- [2021](#2021)
- [2021](#2022)

## 2020 Milestones

High level goals defined for each quarter. We don't go into detail because reasons.

## Q1 OKRs

- [Platform](platform.md) - a cloud platform for microservices development
  * Provides "Micro as a Service"
  * Invite only for community
  * Public cloud hosting (free)
  * Onboard users 10 at a time

## Q2 OKRs

- [Paid product](https://m3o.com) - Fully managed Micro Platform
  * Provides "Micro as a Service" to customers
  * Namespace per customer in Kubernetes
  * Subscription based $35/user/month
  * SaaS Community, Developer and Team tiers

## Q3 OKRs

- M3O Platform MVP - Paid developer tier
  * Invite only
  * Public launch
  * Signup 100 customers
 
## Q4 OKRs

- M3O Team Tier - Collaboration features
  * Shared namespaces
  * Per namespace quotas
  * SLAs
  
## 2021

Delivery by end of July 2021 (Enabled in September)

- [Services](services.md) - Value add Micro services on the Micro platform
  * Free and paid services to consume
  * All services built in [m3o/services](https://github.com/m3o/services)
  * Comprehensive list of [services](services.md)
  * Leveraging existing APIs (Stripe, Twilio, etc)
  * Single API account for providers managed by Micro

Delivery by end of year 2021

- [Network](network.md) - A marketplace to buy, sell and share services
  * Ability to buy and sell services on the Micro platform
  * Transactional model: Percentage per request/service or flat 30%
  * Ability to run external "Resources" such as redis, postgres, etc

## 2022

- License Micro Services
- Otherwise TBD
